import itertools
import numpy as np
import matplotlib.pyplot as plt

# Split dataset into training set and test set (pTest):
def split_dataset(X, Y, pTest):
	X_test = X[:int(len(X) * pTest)]
	Y_test = Y[:int(len(Y) * pTest)]
	X_train  = X[int(len(X) * pTest):]
	Y_train  = Y[int(len(Y) * pTest):]

	return X_train, Y_train, X_test, Y_test

# Compute false positive rate:
def false_positive_rate(Y_true, Y_pred):
	fp = 0 # false positives
	tn = 0 # true negatives

	for yt, yp in zip(Y_true, Y_pred):
		if yt == 0 and yp == 1:
			fp = fp + 1
		elif yt == 0 and yp == 0:
			tn = tn + 1

	return fp / (fp + tn)

# From: http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    np.set_printoptions(precision=2)
    plt.figure(figsize=(4.5, 4), dpi=720, tight_layout=True)
	
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    #plt.show()
    plt.savefig("../images/" + title)
