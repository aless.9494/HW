import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn

def plot_data_histogram_ORIGINAL(class_names, data1):
    
    df = pd.DataFrame({'celltype': class_names, 'Whole Train dataset (Water EXcluded, i.e. false positives)': data1})
    df = df[["celltype", "Whole Train dataset (Water EXcluded, i.e. false positives)"]]
    df.set_index(["celltype"], inplace=True)
    ax = df.plot(kind='barh', width=1)

    plt.xlabel('Number of Samples')
    plt.ylabel('Boat Classes')
    plt.grid(True)

    plt.show()

def plot_data_histogram_SPECIFIC(class_names, data1, data2):
    
    df = pd.DataFrame({'celltype': class_names, 'Train data reduced': data1, 'Train data reduced after boat detection': data2})
    df = df[["celltype", "Train data reduced", "Train data reduced after boat detection"]]
    df.set_index(["celltype"], inplace=True)
    ax = df.plot(kind='barh', width=1)

    plt.xlabel('Number of Samples')
    plt.ylabel('Boat Classes')
    plt.grid(True)

    plt.show()

def plot_data_histogram_GENERAL(class_names, data1, data2):
    
    df = pd.DataFrame({'celltype': class_names, 'Train data reduced': data1, 'Train data reduced after boat detection': data2})
    df = df[["celltype", "Train data reduced", "Train data reduced after boat detection"]]
    df.set_index(["celltype"], inplace=True)
    ax = df.plot(kind='barh', width=1)

    plt.xlabel('Number of Samples')
    plt.ylabel('Boat Classes')
    plt.grid(True)

    plt.show()

def plot_normalized_confusion_matrix(cm, classes, cmap="Reds"):
    
    # NORMALIZE THE CONFUSION MATRIX
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    rows_number = range(0, np.size(cm, 0))
    cols_number = range(0, np.size(cm, 1))
    for i in rows_number:
        for j in cols_number:
            cm[i,j] = round(cm[i,j], 2)
    #print(cm)
    df_cm = pd.DataFrame(cm, index = [i for i in classes],
                             columns = [i for i in classes])

    sn.heatmap(df_cm, annot=True, cmap=cmap)
    plt.xticks(rotation=0) 
    plt.ylabel('Actual Class')
    plt.xlabel('Predicted Class')

    plt.show()

if __name__ == "__main__":

    # ___________ LOADINGS AND USEFULL 'USABLE TYPE CONVERTION'___________
    path_to_data_WITH_detection = "C:\\Users\\damia\\Desktop\\HMW2_1583073\\data_WITH_detection\\"
    path_to_data_WITHOUT_detection = "C:\\Users\\damia\\Desktop\\HMW2_1583073\\data_WITHOUT_detection\\"
    paths_to_data = [path_to_data_WITH_detection, path_to_data_WITHOUT_detection]

    # ------------------------------------------------ LOADINGS AND 'USABLE DATA TYPE CONVERTION': ------------------------------------------------

    # _________________________ Specific original _________________________
    class_names_ORIGINAL = []
    dict_train_samples_per_boat = np.load(path_to_data_WITHOUT_detection + "dict_train_samples_per_boat.npy")
    TRAIN_samples_per_boat_SPECIFIC_ORIGINAL = []
    for key, value in dict_train_samples_per_boat.item().items():
        TRAIN_samples_per_boat_SPECIFIC_ORIGINAL.append(dict_train_samples_per_boat.item()[key])
        class_names_ORIGINAL.append(key)
    # _____________________________________________________________________

    TRAIN_samples_per_boat_SPECIFIC_REDUCED = [[], []] # --> The first list will be refer to the images cropped WITH boat detection, while the second list will refer to the others
    TRAIN_samples_per_boat_GENERAL = [[], []] # --> The first list will be refer to the images cropped WITH boat detection, while the second list will refer to the others
    class_names_SPECIFIC = []
    class_names_GENERAL = []
    count = 0
    for path in paths_to_data:
        path_to_data = path
        # _________________________ Specific after reduction (only classes which more than 100 samples) _________________________
        dict_train_samples_per_boat_SPECIFIC_REDUCED = np.load(path_to_data + "dict_train_samples_per_boat_REDUCED.npy")
        for key, value in dict_train_samples_per_boat_SPECIFIC_REDUCED.item().items():
            TRAIN_samples_per_boat_SPECIFIC_REDUCED[count].append(dict_train_samples_per_boat_SPECIFIC_REDUCED.item()[key])
            if count == 0:
                class_names_SPECIFIC.append(key)
        # _______________________________________________________________________________________________________________________

        # _________________________ General _________________________
        dict_train_samples_per_boat_GENERAL = np.load(path_to_data + "dict_train_samples_per_boat_GENERAL.npy")
        for key, value in dict_train_samples_per_boat_GENERAL.item().items():
            TRAIN_samples_per_boat_GENERAL[count].append(dict_train_samples_per_boat_GENERAL.item()[key])
            if count == 0:
                class_names_GENERAL.append(key)
        # ___________________________________________________________

        count += 1

    # ---------------------------------------------------------------------------------------------------------------------------------------------

    # ____________________________________ PLOTS: ____________________________________

    # Original SPECIFIC classes
    plot_data_histogram_ORIGINAL(class_names_ORIGINAL, TRAIN_samples_per_boat_SPECIFIC_ORIGINAL)

    # Comparison between SPECIFIC classes (reduced ones VS reduced and cropped ones), considering only the classes which have more than 100 samples)
    plot_data_histogram_SPECIFIC(class_names_SPECIFIC, TRAIN_samples_per_boat_SPECIFIC_REDUCED[1], TRAIN_samples_per_boat_SPECIFIC_REDUCED[0])

    # Comparison between GENERAL classes (reduced ones VS reduced and cropped ones):
    plot_data_histogram_GENERAL(class_names_GENERAL, TRAIN_samples_per_boat_GENERAL[1], TRAIN_samples_per_boat_GENERAL[0])




