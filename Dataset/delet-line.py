
"""
with open("/home/alee8/Scrivania/HW2-Keras/src/sc5-2013-Mar-Apr-Test-20130412/bo.txt","r") as input:
    with open("/home/alee8/Scrivania/HW2-Keras/src/sc5-2013-Mar-Apr-Test-20130412/ok.txt","wb") as output: 
        for line in input:
            if line!="*"+"va bene"+"\n":
                output.write(line)
"""

bad_words = ['Snapshot Barca Multipla', 'Snapshot Barca Parziale',
 'Mototopo corto', 'Gondola', 'Lancia: maggiore di 10 m', 'Motopontone rettangolare', 'Motoscafo ACTV', 
   'Sandolo a remi', 'Sanpierota',  'VigilidelFuoco', 'Snapshot Acqua', 'Lancia: fino 10 m\n', 'Polizia', 'Ambulanza', 'Topa', 'Raccolta rifiuti', 'Alilaguna', 'Barchino', 'Motobarca']

with open('/home/alee8/HW/Dataset/sc5-2013-Mar-Apr-Test-20130412/ground_truth.txt') as oldfile:
	with open('/home/alee8/HW/Dataset/sc5-2013-Mar-Apr-Test-20130412/ok.txt', 'w') as newfile:
		for line in oldfile:
			if not any(bad_word in line for bad_word in bad_words):
				newfile.write(line)
