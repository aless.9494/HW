import glob
import numpy as np
import cv2

info_file_train = "/home/alee8/Dataset/sc5/DBinfo.txt"
info_file_test = "/home/alee8/Dataset/sc5-2013-Mar-Apr-Test-20130412/ground_truth.txt" 
count_line = 0
dict_train_samples_per_boat = {}
dict_train_samples_per_boat_GENERAL = {"people transport": 0, "general transport": 0, "pleasure craft": 0, "rowing transport": 0, "public utility": 0}
dict_test_samples_per_boat = {}
dict_test_imageName_to_class = {} # --> It will be use when we will have to keep the order of the association 'image_name.jpg':'class_name' related to the test set
list_of_all_test_classes = [] # --> it will be used to know the occurences of each class within the test set
dict_from_specific_class_to_IDs = {}
# ASSOCIATION BETWEEN GENERAL CLASSES AND SPECIFIC CLASSES
dict_from_general_to_specific_classes = {"people transport":  ["Lanciafino10m",
                                                               "Lanciafino10mBianca",
                                                               "Lanciafino10mMarrone",
                                                               "Lanciamaggioredi10m",
                                                               "Lanciamaggioredi10mBianca",
                                                               "Lanciamaggioredi10mMarrone",
                                                               "Alilaguna",
                                                               "VaporettoACTV",
                                                               "MotoscafoACTV"],
                                         "general transport": ["Motobarca",
                                                               "Mototopo",
                                                               "Raccoltarifiuti",
                                                               "Motopontonerettangolare"],
                                         "pleasure craft":    ["Barchino",
                                                               "Patanella",
                                                               "Sanpierota",
                                                               "Cacciapesca",
                                                               "Topa"],
                                         "rowing transport":  ["Gondola",
                                                               "Sandoloaremi",
                                                               "Caorlina"],
                                         "public utility":    ["Polizia",
                                                               "Ambulanza",
                                                               "VigilidelFuoco"]
                                        }
dict_from_general_classes_to_IDs = {"people transport": 0, "general transport": 1, "pleasure craft": 2, "rowing transport": 3, "public utility": 4}
classes_not_considered = [] # --> Uncomment this part you want to consider only the classes which have more than 'MIN_SAMPLES' samples 
TOTAL_SPECIFIC_CLASSES = 24 # --> 'Water' EXcluded; This value must be changed according to the claffication type (i.e. SPECIFIC, which varies also with 'MIN_SAMPLES', or GENERAL)
TOTAL_GENERAL_CLASSES = 5
TOTAL_TRAIN_SAMPLES = 4774 # --> 'Water' INcluded
TOTAL_TEST_SAMPLES = 1970 # --> 'Water' and others classes (like 'Barcamultipla', ...) INcluded
MIN_SAMPLES = 100 # --> Minimum number of samples which a specific class must have to be considered (this value has to be set only in case of data related to a SPECIFIC classification) 

boat_detection = False
#boat_detection = True # IF YOU WANT TO OBTAIN DATA AND INFO RELATED TO IMAGES DERIVING FROM THE APPLICATION OF THE BOAT DETECTION, THEN UNCOMMENT THESE LINE (and comment the one)

# ANALYZE TRAIN INFO FILE
with open(info_file_train) as f:
    while True: 
        current_line = f.readline().strip()
        count_line += 1
        if count_line >= 8:
            current_line = current_line.split()
            if "Water" in current_line:
                break
            current_specific_class = current_line[1]
            occurrences_per_current_specific_class = int(current_line[0])
            dict_train_samples_per_boat[current_specific_class] = occurrences_per_current_specific_class

            if boat_detection == False:
                if current_specific_class in dict_from_general_to_specific_classes["people transport"]:
                    dict_train_samples_per_boat_GENERAL["people transport"] += occurrences_per_current_specific_class
                elif current_specific_class in dict_from_general_to_specific_classes["general transport"]:
                    dict_train_samples_per_boat_GENERAL["general transport"] += occurrences_per_current_specific_class
                elif current_specific_class in dict_from_general_to_specific_classes["pleasure craft"]:
                    dict_train_samples_per_boat_GENERAL["pleasure craft"] += occurrences_per_current_specific_class
                elif current_specific_class in dict_from_general_to_specific_classes["rowing transport"]:
                    dict_train_samples_per_boat_GENERAL["rowing transport"] += occurrences_per_current_specific_class
                else:
                    dict_train_samples_per_boat_GENERAL["public utility"] += occurrences_per_current_specific_class

# ANALYZE TEST INFO FILE
with open(info_file_test) as f:
    while True:

        current_line = f.readline().strip()
        
        if current_line == "":
            break

        current_line = current_line.split(';')
        current_test_image = current_line[0]
        current_test_class = current_line[1].replace(':', '').replace(' ', '')
        dict_test_imageName_to_class[current_test_image] = current_test_class

path_to_boats_TRAIN_BEFORE_boat_detection = glob.glob("/home/alee8/Dataset/sc5/*")  
path_to_boats_TEST_BEFORE_boat_detection = glob.glob("/home/alee8/Dataset/sc5-2013-Mar-Apr-Test-20130412/*") 

# After the boat detection some images will be obviously lost (we the images deriving from the boat detection into a different path folder)
path_to_boats_TRAIN_AFTER_boat_detection = glob.glob("C://Users//damia//Desktop//ObjDetect//train_detected_boat//*")
path_to_boats_TEST_AFTER_boat_detection = glob.glob("C://Users//damia//Desktop//ObjDetect//test_detected_boat//*")

if boat_detection == False:
    path_to_boats_train = path_to_boats_TRAIN_BEFORE_boat_detection
    path_to_boats_test = path_to_boats_TEST_BEFORE_boat_detection
elif boat_detection == True:
    path_to_boats_train = path_to_boats_TRAIN_AFTER_boat_detection
    path_to_boats_test = path_to_boats_TEST_AFTER_boat_detection

dict_train_samples_per_boat_REDUCED = {} # --> REDUCED stands for 'considering only the (used) classes which has less than MIN_SAMPLES samples'

IMAGE_SIZE = 28
train_images = []
train_labels = []
train_labels_general_classes = []
test_images = []
test_labels = []
test_labels_general_classes = []

current_ID = 0
# TRAIN IMAGES AND LABELS TRANSFORMATION
print("TRAIN images and labels transformation . . .")
for boat in path_to_boats_train:

    boat_name = boat.split('/')
    boat_name = boat_name[len(boat_name)-1]

    # DO NOT CONSIDER THE FALSE POSITIVES AND USELESS FILES
    if (boat_name == "Water") or (boat_name == "DBinfo.txt"): 
        continue

    if dict_train_samples_per_boat[boat_name] < MIN_SAMPLES:
        classes_not_considered.append(boat_name)

    if boat_detection == False:
        # CHECK IF THE CURRENT BOAT CLASS HAS THE MINIMUM NUMBER OF SAMPLES REQUIRED FOR TRAINING
        if dict_train_samples_per_boat[boat_name] > MIN_SAMPLES: # --> Uncomment this part if you want to consider only the classes which have more than 'MIN_SAMPLES' samples
            dict_train_samples_per_boat_REDUCED[boat_name] = dict_train_samples_per_boat[boat_name]
            dict_from_specific_class_to_IDs[boat_name] = current_ID
            current_ID += 1
    elif boat_detection == True:
        if dict_train_samples_per_boat[boat_name] > MIN_SAMPLES: # --> Uncomment this part if you want to consider only the classes which have more than 'MIN_SAMPLES' samples
            dict_from_specific_class_to_IDs[boat_name] = current_ID
            current_ID += 1
        images_per_boat = len(glob.glob(boat + "/*"))
        # CHECK IF THE CURRENT BOAT CLASS HAS THE MINIMUM NUMBER OF SAMPLES REQUIRED FOR TRAINING
        if dict_train_samples_per_boat[boat_name] > MIN_SAMPLES: # --> Uncomment this part if you want to consider only the classes which have more than 'MIN_SAMPLES' samples
            dict_train_samples_per_boat_REDUCED[boat_name] = images_per_boat

        if boat_name in dict_from_general_to_specific_classes["people transport"]:
            dict_train_samples_per_boat_GENERAL["people transport"] += images_per_boat
        elif boat_name in dict_from_general_to_specific_classes["general transport"]:
            dict_train_samples_per_boat_GENERAL["general transport"] += images_per_boat
        elif boat_name in dict_from_general_to_specific_classes["pleasure craft"]:
            dict_train_samples_per_boat_GENERAL["pleasure craft"] += images_per_boat
        elif boat_name in dict_from_general_to_specific_classes["rowing transport"]:
            dict_train_samples_per_boat_GENERAL["rowing transport"] += images_per_boat
        else:
            dict_train_samples_per_boat_GENERAL["public utility"] += images_per_boat

    current_boat_images = glob.glob(boat + "/*")

    for image in current_boat_images:

        if boat_name in dict_from_general_to_specific_classes["people transport"]:
            train_labels_general_classes.append("people transport")
        elif boat_name in dict_from_general_to_specific_classes["general transport"]:
            train_labels_general_classes.append("general transport")
        elif boat_name in dict_from_general_to_specific_classes["pleasure craft"]:
            train_labels_general_classes.append("pleasure craft")
        elif boat_name in dict_from_general_to_specific_classes["rowing transport"]:
            train_labels_general_classes.append("rowing transport")
        else:
            train_labels_general_classes.append("public utility")

        # CHECK IF THE CURRENT BOAT CLASS HAS THE MINIMUM NUMBER OF SAMPLES REQUIRED FOR TRAINING
        if dict_train_samples_per_boat[boat_name] < MIN_SAMPLES: # --> Uncomment this part if you want to consider only the classes which have more than 'MIN_SAMPLES' samples
            continue

        if boat_detection == False:
            dict_train_samples_per_boat_REDUCED[boat_name] = dict_train_samples_per_boat[boat_name]
        elif boat_detection == True:
            images_per_boat = len(glob.glob(boat + "/*"))
            dict_train_samples_per_boat_REDUCED[boat_name] = images_per_boat

        try:
            image = cv2.imread(image)
            image = np.array(image)
        except Exception:
            print("Image Loading Error --> image", image_name)
            continue

        # GRAY CONVERTION
        try:
          image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        except Exception:
            try:
                image = cv2.imdecode(image, cv2.CV_LOAD_IMAGE_GRAYSCALE)
            except Exception:
                print("Gray Conversion Error --> image", image_name)
                continue

        # IMAGE RESIZING
        try:
            image = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE), interpolation = cv2.INTER_CUBIC) / 255. 
        except Exception:
            print("Resizing Error --> image", image_name)
            continue

        train_images.append(image)
        train_labels.append(boat_name)
print("DONE")

# TEST IMAGES AND LABELS TRANSFORMATION
print("\nTEST images and labels transformation . . .")
for image in path_to_boats_test:

    image_name = image.split('/')
    image_name = image_name[len(image_name)-1]

    # DO NOT CONSIDERE USELESS FILES
    if image_name == 'ground_truth.txt' or image_name == 'Thumbs.db':
        continue

    # STORE THE SAMPLES FOR EACH GENERAL CLASS WITHIN TRAIN SET
    if dict_test_imageName_to_class[image_name] in dict_from_general_to_specific_classes["people transport"]:
        test_labels_general_classes.append("people transport")
    elif dict_test_imageName_to_class[image_name] in dict_from_general_to_specific_classes["general transport"]:
        test_labels_general_classes.append("general transport")
    elif dict_test_imageName_to_class[image_name] in dict_from_general_to_specific_classes["pleasure craft"]:
        test_labels_general_classes.append("pleasure craft")
    elif dict_test_imageName_to_class[image_name] in dict_from_general_to_specific_classes["rowing transport"]:
        test_labels_general_classes.append("rowing transport")
    else:
        test_labels_general_classes.append("public utility")

    if dict_test_imageName_to_class[image_name] in classes_not_considered: # --> Uncomment this part if you want to consider only the classes which have more than 'MIN_SAMPLES' samples
        continue

    try:
        image = cv2.imread(image)
        image = np.array(image)
    except Exception:
        print("Image Loading Error --> image", image_name)
        continue

    # GRAY CONVERTION
    try:
      image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    except Exception:
        try:
            image = cv2.imdecode(image, cv2.CV_LOAD_IMAGE_GRAYSCALE)
        except Exception:
            print("Gray Conversion Error --> image", image_name)
            continue

    # IMAGE RESIZING
    try:
        image = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE), interpolation = cv2.INTER_CUBIC) / 255. 
    except Exception:
        print("Resizing Error --> image", image_name)
        continue

    current_test_label = dict_test_imageName_to_class[image_name]
    if current_test_label == "SnapshotAcqua":
        continue 
    elif current_test_label == "SnapshotBarcaMultipla": # --> Do not consider photos with multiple boats (within train set we do not have numplitple boats)
        continue
    elif current_test_label == "SnapshotBarcaParziale": # --> Do not consider photos with partial boats (within train set we do not have partial boats)
        continue
    elif current_test_label == "Mototopocorto": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        continue

    list_of_all_test_classes.append(current_test_label)

    test_images.append(image)
    test_labels.append(current_test_label)
print("DONE")

# STORE THE SAMPLES FOR EACH CLASS WITHIN TEST SET
classes_yet_encountered = []
for label in list_of_all_test_classes:
    if label in classes_yet_encountered:
        continue
    else:
        dict_test_samples_per_boat[label] = list_of_all_test_classes.count(label)
        classes_yet_encountered.append(label)

TOTAL_SPECIFIC_CLASSES_REDUCED = len(dict_train_samples_per_boat_REDUCED); # --> It contains the number of classes which have more than 'MIN_SAMPLES' samples

def one_hot_conversion(index, general_classification):
    global TOTAL_SPECIFIC_CLASSES_REDUCED
    global TOTAL_GENERAL_CLASSES
    if general_classification == False:
        one_hot = np.zeros(TOTAL_SPECIFIC_CLASSES_REDUCED)
    elif general_classification == True:
        one_hot = np.zeros(TOTAL_GENERAL_CLASSES)
    one_hot[index] = 1.0
    
    return one_hot

# THE FOLLOWING FUNCTION SHUFFLE TWO OR THREE VECTORS KEEPING THE INDEX CORRESPONCES BETWEEN THEM 
def shuffle_data(x, y, z=[]):
    if len(x) != len(y):
        print("ERROR: Arguments of function 'shuffle_data' are of different size; They must be vectors of the same dimension!")
        return
    x_shuffled = []
    y_shuffled = []
    shuffled_indeces = range(len(x))
    if z==[]:
        for index in shuffled_indeces:
            x_shuffled.append(x[index])
            y_shuffled.append(y[index])

        return x_shuffled, y_shuffled
    else:
        z_shuffled = []
        for index in shuffled_indeces:
            x_shuffled.append(x[index])
            y_shuffled.append(y[index])
            z_shuffled.append(z[index])

        return x_shuffled, y_shuffled, z_shuffled

# TRAIN LABELS ONE-HOT ENCODING
one_hot_train_labels = []
print(dict_from_specific_class_to_IDs)
for label in train_labels:
    one_hot_train_labels.append(one_hot_conversion(dict_from_specific_class_to_IDs[label], general_classification=False))

# TRAIN LABELS ONE-HOT ENCODING FOR GENERAL CLASSES
one_hot_train_GENERAL_labels = [] 
for label in train_labels_general_classes:
    one_hot_train_GENERAL_labels.append(one_hot_conversion(dict_from_general_classes_to_IDs[label], general_classification=True))

# TEST LABELS ONE-HOT ENCODING
one_hot_test_labels = [] 
for label in test_labels:
    one_hot_test_labels.append(one_hot_conversion(dict_from_specific_class_to_IDs[label], general_classification=False))

# TEST LABELS ONE-HOT ENCODING FOR GENERAL CLASSES
one_hot_test_GENERAL_labels = [] 
for label in test_labels_general_classes:
    one_hot_test_GENERAL_labels.append(one_hot_conversion(dict_from_general_classes_to_IDs[label], general_classification=True))

print("\nSHUFFLING DATA . . .")
train_images_SHUFF, one_hot_train_labels_SHUFF = shuffle_data(train_images, one_hot_train_labels, [])
test_images_SHUFF, one_hot_test_labels_SHUFF = shuffle_data(test_images, one_hot_test_labels, []) 
print("DONE . . .")

# SAVINGS
print("\nSAVING INPUT DATA AND OTHER INFO . . .")
# TRAIN images and labels
np.save("train_images.npy", train_images_SHUFF)
np.save("train_labels.npy", one_hot_train_labels_SHUFF)
#np.save("one_hot_train_GENERAL_labels.npy", one_hot_train_GENERAL_labels_SHUFF)

# TEST images and labels
np.save("test_images.npy", test_images_SHUFF)
np.save("test_labels.npy", one_hot_test_labels_SHUFF)
#np.save("one_hot_test_labels_GENERAL.npy", one_hot_test_labels_GENERAL_SHUFF)

# TRAIN samples number for plotting
np.save("dict_train_samples_per_boat.npy", dict_train_samples_per_boat)
np.save("dict_train_samples_per_boat_REDUCED.npy", dict_train_samples_per_boat_REDUCED)

# TEST samoles number for plotting
np.save("dict_test_samples_per_boat.npy", dict_test_samples_per_boat)
np.save("dict_train_samples_per_boat_GENERAL.npy", dict_train_samples_per_boat_GENERAL)

# Other info
np.save("dict_from_specific_class_to_IDs.npy", dict_from_specific_class_to_IDs)
np.save("dict_from_general_classes_to_IDs.npy", dict_from_general_classes_to_IDs)
np.save("TOTAL_SPECIFIC_CLASSES_REDUCED.npy", TOTAL_SPECIFIC_CLASSES_REDUCED)
np.save("TOTAL_GENERAL_CLASSES.npy", TOTAL_GENERAL_CLASSES)
print("DONE\n")

print("\n-------------------------- INFO --------------------------\n")

print("TOTAL CLASSES ('Water', i.e. false positives, EXcluded):", TOTAL_SPECIFIC_CLASSES, "\n")
print("TOTAL CLASSES ('Water', i.e. false positives, EXcluded) which have more than", MIN_SAMPLES, "samples:", TOTAL_SPECIFIC_CLASSES_REDUCED, "\n")
print("TOTAL TRAIN SAMPLES ('Water', i.e. false positives, INcluded):", TOTAL_TRAIN_SAMPLES, "\n")
print("TOTAL TRAIN SAMPLES WITH MORE THAN", MIN_SAMPLES,"SAMPLES PER CLASS ('Water', i.e. false positives, EXcluded):", len(train_images), "\n")
print("TOTAL TEST SAMPLES ('Water', i.e. false positives, INcluded):", TOTAL_TEST_SAMPLES, "\n")
print("TOTAL TEST SAMPLES ('Water', i.e. false positives, EXcluded) which have more than", MIN_SAMPLES, "samples:", len(test_images), "\n")

print("________________ TRAIN ________________\n")

print("Specific classes:\n")
print("OCCURRENCES FOR EACH SPECIFIC CLASS WITHIN TRAIN SET:", "\n")
print(dict_train_samples_per_boat, "\n")
print("OCCURRENCES FOR EACH SPECIFIC CLASS (considering only the ones which have more than 'MIN_SAMPLES' samples) WITHIN TRAIN SET:")
print(dict_train_samples_per_boat_REDUCED, "\n")

print("General classes:", "\n")
print("OCCURRENCES FOR EACH GENERAL CLASS WITHIN TRAIN SET:", "\n")
print(dict_train_samples_per_boat_GENERAL, "\n")

print("_______________________________________\n")

print("________________ TEST ________________\n")

print("OCCURRENCES FOR EACH SPECIFIC CLASS WITHIN TEST SET (considering also the ones which have less than 'MIN_SAMPLES' samples):")
print(dict_test_samples_per_boat, "\n")
print("______________________________________\n")

print("----------------------------------------------------------")
print (len(train_images))
