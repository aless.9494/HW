import glob
import numpy as np
import os


dict_test_imageName_to_class = {} # --> It will be use when we will have to keep the order of the association 'image_name.jpg':'class_name' related to the test set

# ----------------------------------------------- MODIFICA QUESTI PATHS ----------------------------------------------------------- 

path_to_boats_test = glob.glob("/home/alee8/HW/Dataset/sc5-2013-Mar-Apr-Test-20130412/*") 
info_file_test = "/home/alee8/HW/Dataset/sc5-2013-Mar-Apr-Test-20130412/ground_truth.txt"

# ---------------------------------------------------------------------------------------------------------------------------------

# ANALYZE TEST INFO FILE
with open(info_file_test) as f:
    while True:

        current_line = f.readline().strip()
        
        if current_line == "":
            break

        current_line = current_line.split(';')
        current_test_image = current_line[0]
        current_test_class = current_line[1].replace(':', '').replace(' ', '')
        #if current_test_class == "SnapshotBarcaMultipla"
        
        dict_test_imageName_to_class[current_test_image] = current_test_class
print (len(dict_test_imageName_to_class))
# TEST IMAGES AND LABELS TRANSFORMATION
for image in path_to_boats_test:

    image_name = image.split('/')
    image_name = image_name[len(image_name)-1]

    # DO NOT CONSIDER USELESS FILES
    if image_name == 'ground_truth.txt' or image_name == 'Thumbs.db':
        continue

    current_test_label = dict_test_imageName_to_class[image_name]
    if current_test_label == "SnapshotAcqua":
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    if current_test_label == "SnapshotBarcaMultipla": # --> Do not consider photos with multiple boats (within train set we do not have numplitple boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "SnapshotBarcaParziale": # --> Do not consider photos with partial boats (within train set we do not have partial boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Mototopocorto": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
            
            
    elif current_test_label == "Gondola": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Lanciamaggioredi10mBianca": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Lanciafino10m": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Lanciamaggioredi10mMarrone": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Motopontonerettangolare": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "MotoscafoACTV": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Sandoloaremi": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue  
    elif current_test_label == "Sanpierota": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue  
    elif current_test_label == "VigilidelFuoco": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue 
    elif current_test_label == "Caorlina": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue  
    elif current_test_label == "Cacciapesca": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue   
            #12 classi rimanenti fino ad ora
    elif current_test_label == "Topa": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue   
    elif current_test_label == "Polizia": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
    elif current_test_label == "Raccoltarifiuti": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue   
    elif current_test_label == "Ambulanza": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue
            
    elif current_test_label == "Alilaguna": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue  
    elif current_test_label == "Barchino": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue  

    elif current_test_label == "Motobarca": # --> Do not consider photos with Mototopocorto boats (within train set we do not have Mototopocorto boats)
        try: 
            os.remove(image)
        except:
            print("Errore durante l'eliminazione dell'immagine", image)
            continue        
print (len(dict_test_imageName_to_class))
print("DONE")
