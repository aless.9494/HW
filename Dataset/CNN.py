import tensorflow as tf
import numpy as np
import plottings
from plottings import plot_normalized_confusion_matrix

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

def maxpool2d(x):
    #                        size of window         movement of window
    return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

def convolutional_neural_network(x):
    weights = {'W_conv1':tf.Variable(tf.random_normal([5,5,1,32])),
               'W_conv2':tf.Variable(tf.random_normal([5,5,32,64])),
               'W_fc':tf.Variable(tf.random_normal([7*7*64,1024])),
               'out':tf.Variable(tf.random_normal([1024, N_CLASSES]))}

    biases = {'b_conv1':tf.Variable(tf.random_normal([32])),
               'b_conv2':tf.Variable(tf.random_normal([64])),
               'b_fc':tf.Variable(tf.random_normal([1024])),
               'out':tf.Variable(tf.random_normal([N_CLASSES]))}

    x = tf.reshape(x, shape=[-1, 28, 28, 1])

    conv1 = tf.nn.relu(conv2d(x, weights['W_conv1']) + biases['b_conv1'])
    conv1 = maxpool2d(conv1)
    
    conv2 = tf.nn.relu(conv2d(conv1, weights['W_conv2']) + biases['b_conv2'])
    conv2 = maxpool2d(conv2)

    fc = tf.reshape(conv2,[-1, 7*7*64])
    fc = tf.nn.relu(tf.matmul(fc, weights['W_fc'])+biases['b_fc'])
    
    # DROPOUT
    fc = tf.nn.dropout(fc, keep_rate)
    

    output = tf.matmul(fc, weights['out'])+biases['out']

    return output

def train_neural_network(x):
    prediction = convolutional_neural_network(x)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels=y))
    optimizer = tf.train.AdamOptimizer().minimize(cost)
    _, training_accuracy = tf.metrics.accuracy(labels=tf.argmax(y, 1), predictions=tf.argmax(prediction, 1)) # --> returns 2 value and I care only about the first one, i.e. the accuracy updates
    
    # Create the scalar summary (in order to visualize the Loss and Training Accuracy functions graphs and Input Images via Tensorboard):
    first_summary = tf.summary.scalar(name='Loss', tensor=cost)
    second_summary = tf.summary.scalar(name='Training Accuracy', tensor=training_accuracy)

    # summary.image must be a 4-D tensor (RGBA --> if we have grayscale images, then A =1.0, i.e. one channel, while if we have color images, then A = 3, i.e. three channels)
    x_reshaped = tf.reshape(x, (-1, 28, 28, 1)) # --> We will have a tennsor of length -1,28,28 with 1 channel (i.e. whaterver number of grayscale images of size 28x28) 

    third_summary = tf.summary.image(name='Input Images', tensor=x_reshaped, max_outputs=MAX_OUTPUT_IMAGES)

    # Merge all summaries
    merged = tf.summary.merge_all()

    hm_epochs = 100
    with tf.Session() as sess:

        # Create the writer inside the session (the folder 'graphs' will contain the event file related to Tensorboard):
        writer = tf.summary.FileWriter('./graphs', sess.graph)

        sess.run(tf.local_variables_initializer()) # --> Since metrics.accuracy creates two local variables total and count, we need to call local_variables_initializer() to initialize them
        sess.run(tf.global_variables_initializer())

        print("\nTRAINING STARTS . . .\n")

        for epoch in range(hm_epochs):
            epoch_loss = 0
            i=0
            while i < len(train_x):
                start = i
                end = i+BATCH_SIZE
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])

                _, c, train_accuracy = sess.run([optimizer, cost, training_accuracy], feed_dict={x: batch_x,
                                                                                                 y: batch_y})

                # Evaluate alle the summaries:
                summary = merged.eval({x:batch_x, y:batch_y})

                # Add the summary to the writer (i.e. to the event file contained in 'graphs' folder)
                writer.add_summary(summary, epoch)

                epoch_loss += c
                i+=BATCH_SIZE
                
            print('| Epoch', epoch+1, 'completed out of', hm_epochs,'| loss:', round(epoch_loss, 3), '| training accuracy:', round(train_accuracy, 3))

        print("\nTRAINING ENDS")

        predictions = tf.argmax(prediction, 1)
        classes = tf.argmax(y, 1)
        correct = tf.equal(predictions, classes)
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))

        print('\nTesting Accuracy:',accuracy.eval({x:test_x, y:test_y}))

        confusion_matrix = tf.confusion_matrix(classes, predictions)
        c_m = confusion_matrix.eval({x:test_x, y:test_y})
        
        return c_m

if __name__ == "__main__":

    # LOADING DATA 
    # PATHs to the directories which will contain the data related to images WITH and WITHOUT application of boat detection (you have to create manually the following folders and put data properly inside them)
    path_to_data_WITHOUT_boat_detection = "./data_WITHOUT_detection"
    path_to_data_WITH_boat_detection = "./data_WITH_detection" #Queste?
    
    path_to_data = path_to_data_WITHOUT_boat_detection
    #path_to_data = path_to_data_WITH_boat_detection # --> If you want to train on images on which has been applied boat detection, then uncomment this line and comment the previous one

    # _____________________________________________ Data on which has NOT been applied the boat detection: _____________________________________________

    train_x = np.load(path_to_data + "/content/Dataset/train_images.npy")
    train_x = train_x.reshape(train_x.shape[0], -1)

    train_y = np.load(path_to_data + "\\train_labels.npy")
    #train_y = np.load(path_to_data + "/content/Dataset/one_hot_train_GENERAL_labels.npy") # --> If you want to perform a GENERAL classification uncomment this line and comment the prevoius one 

    test_x = np.load(path_to_data + "/content/Dataset/test_images.npy")
    test_x = test_x.reshape(test_x.shape[0], -1)

    test_y = np.load(path_to_data + "\\test_labels.npy")
    #test_y = np.load(path_to_data + "/content/Dataset/one_hot_test_labels_GENERAL.npy") # --> If you want to perform a GENERAL classification uncomment this line and comment the prevoius one 

    dict_from_specific_class_to_IDs = np.load(path_to_data + "/content/Dataset/dict_from_specific_class_to_IDs.npy")
    dict_from_general_classes_to_IDs = np.load(path_to_data + "/content/Dataset/dict_from_general_classes_to_IDs.npy")

    dict_from_class_to_IDs = dict_from_specific_class_to_IDs
    #dict_from_class_to_IDs = dict_from_general_classes_to_IDs # --> If you want to perform a GENERAL classification uncomment this line and comment the prevoius one

    N_CLASSES = np.load(path_to_data + "\\TOTAL_SPECIFIC_CLASSES_REDUCED.npy")
    #N_CLASSES = np.load(path_to_data + "/content/Dataset/TOTAL_GENERAL_CLASSES.npy") # --> If you want to perform a GENERAL classification uncomment this line and comment the prevoius one

    # ___________________________________________________________________________________________________________________________________________________

    # ___________________________________________________________________________________________________________________________________________________

    for key, value in dict_from_class_to_IDs.item().items(): # --> Add 1 to every key value, because in the confusion matrix we start from 1 (acting in this way we can have a proper ID/class correspondence)
        dict_from_class_to_IDs.item()[key] += 1

    class_IDs = range(1, N_CLASSES+1)
    BATCH_SIZE = 128
    MAX_OUTPUT_IMAGES = 3

    x = tf.placeholder('float', [None, 784])
    y = tf.placeholder('float')

    keep_rate = 0.8 # --> It will be used in the DROPOUT
    keep_prob = tf.placeholder(tf.float32)

    correspondences_classes_IDs = dict_from_specific_class_to_IDs
    #correspondences_classes_IDs = dict_from_general_classes_to_IDs # --> If you want to perform a GENERAL classification uncomment this line and comment the prevoius one

    confusion_matrix = train_neural_network(x)
    print("\nCORRESPONDECES BETWEEN CLASSES AND IDs:\n", correspondences_classes_IDs)
    
    plot_normalized_confusion_matrix(confusion_matrix, classes=class_IDs)

